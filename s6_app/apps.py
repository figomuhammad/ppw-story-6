from django.apps import AppConfig


class S6AppConfig(AppConfig):
    name = 's6_app'
