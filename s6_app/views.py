from django.shortcuts import render
from .forms import StatusForm
from .models import Status


def index(request):
    statusList = Status.objects.all()
    
    if request.method == 'POST':
        statusForm = StatusForm(request.POST)

        if statusForm.is_valid():
            statusForm.save()

    statusForm = StatusForm()
    return render(request, 'index.html', {
        'statusForm': statusForm,
        'statusList': statusList,
    })
