from django.test import TestCase
from django.urls import resolve
from .models import Status
from .views import index
from .forms import StatusForm


class S6AppUnitTest(TestCase):
    # Templates
    def testIndexPageIsExist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def testIndexPageUsesIndexHTML(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testIndexPageUsingIndexFunc(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def testIndexPageContainsHelloHowAreYou(self):
        response = self.client.get('/')
        self.assertIn("Hello, how are you?", response.content.decode('utf8'))

    def testIndexPageHasForm(self):
        response = self.client.get('/')
        self.assertIn("<form", response.content.decode('utf8'))
        self.assertIn("</form>", response.content.decode('utf8'))
        self.assertIsInstance(response.context['statusForm'], StatusForm)

    def testIndexPageHasFormField(self):
        response = self.client.get('/')
        self.assertIn("<input", response.content.decode('utf8'))

    def testIndexPageHasSubmitButton(self):
        response = self.client.get('/')
        self.assertIn('<button type="submit"', response.content.decode('utf8'))
        self.assertIn("</button>", response.content.decode('utf8'))

    def testIndexPageFormDataIsSubmitted(self):
        response = self.client.post('/', data={'message': 'Test'})
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

    def testIndexPageHasStatusList(self):
        response = self.client.get('/')
        self.assertIn('<ul class="list-group', response.content.decode('utf8'))
        self.assertIn("</ul>", response.content.decode('utf8'))

    def testIndexPageStatusDataIsDisplayedWhenFormIsSubmitted(self):
        response = self.client.post('/', data={'message': 'Test'})
        self.assertIn('Test', response.content.decode('utf8'))

    # Models
    def testModelStatusCanCreateObject(self):
        Status.objects.create(message='Test')
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

    def testModelStatusReturnMessageAttribute(self):
        obj = Status.objects.create(message='Test')
        self.assertEqual(str(obj), 'Test')
