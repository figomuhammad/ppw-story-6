from django.db import models


class Status(models.Model):
    message = models.CharField(max_length=200)

    def __str__(self):
        return self.message
